# SQL import test

import csv
import sqlite3

with sqlite3.connect("new.db") as conn:
  c = conn.cursor()

  # Create a variable with csv data from employees.csv file
  employees = csv.reader(open("employees.csv", "rU"))

  # Create a new table to hold the data
  c.execute("CREATE TABLE employees(firstname TEXT, lastname TEXT)")

  # Insert data
  c.executemany("INSERT INTO employees(firstname, lastname) values (?, ?)", employees)
