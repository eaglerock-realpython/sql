import sqlite3
# Happy Day, SQLLite

# Establish DB connection to file
my_conn = sqlite3.connect("new.db")

# Get a cursor object for SQL commands and queries
my_cursor = my_conn.cursor()

# Make a table
my_cursor.execute("""CREATE TABLE population
                    (city TEXT, state TEXT, population INT)
                    """)

# Close DB connection
my_conn.close()
