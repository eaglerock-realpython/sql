import sqlite3

with sqlite3.connect("new.db") as my_conn:
  c = my_conn.cursor()
  c.execute("CREATE TABLE inventory (make TEXT, model TEXT, quantity INT)")

