import sqlite3

func = {
    1: 'avg',
    2: 'max',
    3: 'min',
    4: 'sum'
}

prompt = """Please select a query:
1) Average of numbers
2) Maximum of numbers
3) Minimum of numbers
4) Sum of numbers
5) Quit
Your choice? """

with sqlite3.connect("int.db") as my_conn:
    c = my_conn.cursor()

    quit = False
    while not quit:
        try:
            choice = int(input(prompt))
        except ValueError:
            print("\nChoice was not a number...try again!\n")
            continue

        if choice == 5:
            quit = True

        elif choice >= 1 and choice <= 4:
            try:
                c.execute("SELECT {}(int) FROM integers".format(func[choice]))
            except sqlite3.OperationalError as e:
                print("\nERROR: {}".format(e))
                continue

            output = c.fetchone()
            print("\n{}: {}\n".format(func[choice].upper(), output[0]))

        else:
            print("\nInvalid choice...try again!\n")
