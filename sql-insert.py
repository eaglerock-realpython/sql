import sqlite3
import random

# Let's make the pseudo-randomness a little less pseudo
random.seed()

# Open the dbfile and establish a connection
with sqlite3.connect("int.db") as my_conn:
    # Set up cursor to execute SQL
    c = my_conn.cursor()

    # Make the integers table
    c.execute("DROP TABLE if exists integers")
    c.execute("CREATE TABLE integers (int INT)")

    # Add 100 random integers to the table
    for i in range(100):
        rand = random.randint(0, 100)
        c.execute("INSERT INTO integers VALUES(?)", (rand,))

    # Query the entire integers table
    c.execute("SELECT * FROM integers")
    output = c.fetchall()
    print("Numbers Generated:")

    # Loop through the output
    for i in output:
        # Output is in form of a tuple, so pull the first and only element
        print(i[0], end=' ')        # No \n between numbers

    print("")
