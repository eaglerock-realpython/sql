import sqlite3

with sqlite3.connect("new.db") as conn:
    c = conn.cursor()

    c.execute("UPDATE inventory SET quantity = 7 WHERE model = 'CRV'")
    c.execute("UPDATE inventory SET quantity = 20 WHERE model = 'Focus'")

    print("New Quantities:")
    my_result = c.execute("SELECT * FROM inventory")
    for r in my_result:
        print(r[0], r[1], r[2])

    print("\nFords:")
    my_fords = c.execute("SELECT * FROM inventory WHERE make='Ford'")
    for r in my_fords:
        print(r[1], r[2])
