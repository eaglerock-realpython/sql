import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    # Dict with common sql queries
    sql = {'average': "SELECT avg(population) FROM population",
           'maximum': "SELECT max(population) FROM population",
           'minimum': "SELECT min(population) FROM population",
           'sum': "SELECT sum(population) FROM population",
           'count': "SELECT count(city) FROM population"}

    # Run each of the queries and return output
    for keys, values in sql.items():
        c.execute(values)
        result = c.fetchone()
        print("{}: {}".format(keys, result[0]))
