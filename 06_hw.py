import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    queries = {
        'Focus': "SELECT count(make) FROM orders WHERE model='Focus'",
        'Mustang': "SELECT count(order_date) FROM orders WHERE model='Mustang'",
        'CRV': "SELECT count(order_date) FROM orders WHERE model='CRV'",
        'Civic': "SELECT count(order_date) FROM orders WHERE model='Civic'",
        'Pilot': "SELECT count(order_date) FROM orders WHERE model='Pilot'"
    }

    for key, value in queries.items():
        c.execute(value)
        my_result = c.fetchone()
        print("{} count: {}".format(key, my_result[0]))
