import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    c.execute("CREATE TABLE orders (make TEXT, model TEXT, order_date TEXT)")

    my_orders = [
        ('Honda', 'Civic', '2018-10-15'),
        ('Honda', 'Civic', '2018-11-11'),
        ('Honda', 'Civic', '2019-01-01'),
        ('Honda', 'CRV', '2018-11-19'),
        ('Honda', 'CRV', '2018-11-20'),
        ('Honda', 'CRV', '2018-12-26'),
        ('Honda', 'Pilot', '2018-05-05'),
        ('Honda', 'Pilot', '2018-09-15'),
        ('Honda', 'Pilot', '2018-10-27'),
        ('Ford', 'Mustang', '2018-04-21'),
        ('Ford', 'Mustang', '2018-07-20'),
        ('Ford', 'Mustang', '2018-12-01'),
        ('Ford', 'Focus', '2018-10-15'),
        ('Ford', 'Focus', '2018-10-15'),
        ('Ford', 'Focus', '2019-01-04')
    ]

    c.executemany("INSERT INTO orders(make, model, order_date) VALUES (?, ?, ?)", my_orders)
