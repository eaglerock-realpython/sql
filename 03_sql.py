# Using executemany()

import sqlite3

with sqlite3.connect("new.db") as conn:
  c = conn.cursor()

  # Cities Tuple
  cities = [
    ('Boston', 'MA', 600000),
    ('Chicago', 'IL', 2700000),
    ('Houston', 'TX', 2100000),
    ('Phoenix', 'AZ', 1500000)
    ]

  # Insert the Data into the table using executemany()
  c.executemany('INSERT INTO population VALUES(?, ?, ?)', cities)
