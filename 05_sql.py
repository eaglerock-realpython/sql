import sqlite3

my_conn = sqlite3.connect("new.db")

c = my_conn.cursor()

try:
    c.execute("INSERT INTO populations VALUES('New York City', 'NY', 8400000)")
    c.execute("INSERT INTO populations VALUES('San Francisco', 'CA', 800000)")
    my_conn.commit()
except sqlite3.OperationalError as my_error:
    print("Ooopsie Woopsies, we had an error: {}".format(my_error))

my_conn.close()
