import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    cars = [
        ('Honda', 'CRV', 12),
        ('Honda', 'Civic', 25),
        ('Honda', 'Pilot', 7),
        ('Ford', 'Focus', 30)
    ]

    c.executemany("INSERT INTO inventory(make, model, quantity) VALUES (?, ?, ?)", cars)

    print("Results:")
    print("--------")

    result = c.execute("SELECT * FROM inventory;")

    for row in result:
        print(row[0], row[1], row[2])
