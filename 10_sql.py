import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    c.execute("""
        SELECT population.city, population.population, population.state, regions.region
        FROM population, regions
        WHERE population.city = regions.city
        ORDER by population.city ASC
    """)
    rows = c.fetchall()

    for r in rows:
        print("City: {}".format(r[0]))
        print("Population: {}".format(r[1]))
        print("State: {}".format(r[2]))
        print("Region: {}\n".format(r[3]))
