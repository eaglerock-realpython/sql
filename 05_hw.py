import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    c.execute("""
        SELECT inventory.make, inventory.model,
            inventory.quantity, orders.order_date
        FROM inventory
        INNER JOIN orders
        ON inventory.model = orders.model
    """)
    my_orders = c.fetchall()

    for r in my_orders:
        print("{} {}".format(r[0], r[1]))
        print("{}".format(r[2]))
        print("{}".format(r[3]))
        print("")
