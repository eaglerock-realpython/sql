import sqlite3

with sqlite3.connect("new.db") as my_conn:
    c = my_conn.cursor()

    c.execute("SELECT * FROM inventory;")
    my_inv = c.fetchall()

    for my_make, my_model, my_count in my_inv:
        c.execute("SELECT count(order_date) FROM orders WHERE orders.make = ? AND orders.model = ?", (my_make, my_model))
        my_orders = c.fetchone()

        print("Vehicle: {} {}".format(my_make, my_model))
        print("Inventory: {}".format(my_count))
        print("Orders: {}".format(my_orders[0]))
